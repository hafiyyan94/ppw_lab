from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'lab3html.html')

def about(request):
    return render(request, 'about.html')

def myrecords(request):
    return render(request, 'myrecords.html')

def lifeinphotos(request):
    return render(request, 'lifeinphotos.html')

def blog(request):
    return render(request, 'blog.html')

def visualdesign(request):
    return render(request, 'visdes.html')

def guestform(request):
    return render(request, 'formtamu.html')
